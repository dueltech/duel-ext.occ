/* eslint-disable */
define(
  //-------------------------------------------------------------------
  // DEPENDENCIES
  //-------------------------------------------------------------------
  [
    "jquery", "knockout", "pubsub", "ccDate", "ccLogger",
    "ccResourceLoader!global/utils",
    "viewModels/dynamicProperty", "pageLayout/product"
  ],

  //-------------------------------------------------------------------
  // MODULE DEFINITION
  //-------------------------------------------------------------------
  function ($, ko, pubsub, ccDate, ccLogger, utils, DynamicProperty, Product) {
    'use strict';

    var fetchingScript = false;
    var waitForProduct = null;

    //-------------------------------------------------------------------
    // Constants
    //-------------------------------------------------------------------
    var WIDGET_ID = "duelPhotoGallery";

    //-------------------------------------------------------------------
    // private Functions
    //-------------------------------------------------------------------
    function getDynamicPropertyValue(viewModel, id, defaultValue) {
      if (!viewModel) throw new Error("Invalid view model argument");

      var dp = utils.getDynamicProperty(viewModel, id);
      if (dp) {
        var value = dp.value();
        if (value == null && arguments.length > 2) {
          value = defaultValue;
        }
        return value;
      }

      if (arguments.length > 2) {
        return defaultValue;
      }

      return null;
    }
    
    function loadDuelVision(config) {
      var duelvision = $('#duelvision-component');
      if (duelvision.length === 0) {
        // no duel gallery container present
        return;
      } else if (!duelvision.is(':empty')) {
        if (
          config.product &&
          duelvision[0].firstChild.src.indexOf(config.product) === -1
        ) {
          // product changed and gallery needs to be emptied
          duelvision.empty();
        } else {
          // current product gallery has been rendered alreadyc
          return;
        }
      }
      DuelVision.load(config);
    }

    //-------------------------------------------------------------------
    // View model
    //-------------------------------------------------------------------

    return {

      //-------------------------------------------------------------------
      // Observables & Dynamic Properties
      //-------------------------------------------------------------------

      setDynamicProperty: function (viewModel, id, value) {
        if (!viewModel) throw new Error("Invalid view model argument");
        if (!id || !id.trim()) throw new Error("Invalid id argument");

        var dp = utils.getDynamicProperty(viewModel, id);
        if (!dp) {
          ccLogger.debug("Adding DynamicProperty", [dp.id(), dp.value()]);
          dp = new DynamicProperty();
          dp.id(id);
          viewModel.dynamicProperties.push(dp);
        } else {
          ccLogger.debug("Setting DynamicProperty", [dp.id(), dp.value()]);
        }

        dp.value(value);

        return dp;
      },

      //-------------------------------------------------------------------
      // Lifecycle
      //-------------------------------------------------------------------
      currentProductId: ko.observable(),
      currentPageDomain: ko.observable(),
      duelShortId: ko.observable(),

      beforeAppear: function (page) {
        var widget = this;
        var pageURL = window.location;
        var pageDomain = window.location.origin;
        var shortId = widget.site().extensionSiteSettings.duelsettings.duelShortId;

        widget.currentPageDomain(pageDomain);
        widget.duelShortId(shortId);
        return;
      },

      loadProductGallery: function (widget) {
        this.loadGallery(widget);
        $.Topic(pubsub.topicNames.PAGE_READY).subscribe(
          function (page) {
            if (waitForProduct && waitForProduct.id() === page.contextId) {
              if ($('#duelvision-component').length === 0) {
                // should never happen, but if it does, we work around it with a timeout
                setTimeout(function () { this.loadGallery(widget, product); }.bind(this), 1000);
              } else {
                this.loadGallery(widget, waitForProduct);
              }
              waitForProduct = null;
            }
          }.bind(this)
        );
        $.Topic(pubsub.topicNames.PRODUCT_VIEWED).subscribe(
          function (product) {
            if ($('#duelvision-component').length === 0) {
              // gallery container  not found -> wait for entire page to be ready
              waitForProduct = product;
            } else {
              this.loadGallery(widget, product);
            }
          }.bind(this)
        );
      },

      loadGallery: function (widget, product) {
        // load gallery settings from widget
        var sort = 'sortOrder' in widget ? widget.sortOrder() : 'created';
        var layoutStyle = 'layoutStyle' in widget ? widget.layoutStyle() : 'grid';
        var config = {
          sort: sort,
          color: widget.textColor() || '#222222',
          bgColor: widget.backgroundColor() || '#ffffff',
          layoutStyle: layoutStyle
        };

        if ('galleryRows' in widget && +widget.galleryRows() > 0) {
          config.rows = +widget.galleryRows();
        }
        if ('galleryColumns' in widget && +widget.galleryColumns() > 0) {
          config.columns = +widget.galleryColumns();
        }

        var collectionId = widget.site().extensionSiteSettings.duelsettings.duelCollectionId;
        var shortId = widget.site().extensionSiteSettings.duelsettings.duelShortId;

        if (!product && widget.customGalleryId() !== '') {
          config.id = widget.customGalleryId();
        } else if (
          !product &&
          collectionId &&
          'product' in widget &&
          widget.product() &&
          (!('duelGalleryEnabled' in widget.product()) || widget.product().duelGalleryEnabled())
        ) {
          product = widget.product();
        }

        if (!config.id && product) {
          widget.currentProductId(product.repositoryId());
          var categories = product.parentCategories();
          var isDuel = false;
          for (var i = 0; i < categories.length; i++) {
            if (categories[i].repositoryId() === collectionId) {
              isDuel = true;
            }
          }

          // fetch gallery from shop's short id and product id
          if (isDuel) {
            config.product = shortId + '/' + product.id();
          }

          // get product specific settings (if there are any)
          if ('duelGalleryColor' in product && product.duelGalleryColor()) {
            config.color = product.duelGalleryColor();
          }
          if ('duelGalleryBackgroundColor' in product && product.duelGalleryBackgroundColor()) {
            config.bgColor = product.duelGalleryBackgroundColor();
          }
          if ('duelGalleryRows' in product && +product.duelGalleryRows() > 0) {
            config.rows = +product.duelGalleryRows();
          }
          if ('duelGalleryColumns' in product && +product.duelGalleryColumns() > 0) {
            config.columns = +product.duelGalleryColumns();
          }
          if ('duelGallerySortOrder' in product && product.duelGallerySortOrder()) {
            config.sort = product.duelGallerySortOrder();
          }
          if ('duelGalleryLayoutStyle' in product && product.duelGalleryLayoutStyle()) {
            config.layoutStyle = product.duelGalleryLayoutStyle();
          }
        }
        
        // overwrite config with settings from the advancedSettings field
        if ('advancedSettings' in widget && widget.advancedSettings() !== '') {
          try {
            var advanced = JSON.parse(widget.advancedSettings());
            config = $.extend(config, advanced);
          } catch (error) {
            console.error('Invalid format for advanced Duel gallery settings, should be JSON.');
          }
        }

        if ((config.id || config.product) && !fetchingScript) {
          // load script that inserts correct gallery into #duelvision-component in display.template
          if (!window.DuelVision) {
            fetchingScript = true;
            $.getScript('https://vision.duel.me/loader.js', function () {
              fetchingScript = false;
              loadDuelVision(config);
            });
          } else {
            loadDuelVision(config);
          }
        }
      }
    };
  }
);