# Duel Gallery Integration v2

The second version of the Duel gallery integration for Oracle Commerce Cloud. You also need this extension in order for the Duel server-side extension to work.

## Steps to setup the installation

1. Create a new extension ID in Settings > Extensions > Developer.
2. Replace **extensionID** in `ext.json` with the one you generated in step 1.
3. Zip all files (not the folder!), excluding the readme & gitignore and upload the extension (run `zip -r duel-oracle.zip config widget ext.json` in the working directory).

## Steps to configure the extension

1. Navigate to Settings > Duel Settings and add your Duel Short id.
2. Create or choose an existing collection with the products that are connected to Duel and select it in Settings > Duel Settings.
3. Go to Design, find the Product Layout, go into Grid View and drag the Duel Gallery component into the layout.

## Customization

You can adjust the default colors, row & column layout, layout style and sort order for each widget in its settings. There's also the possibility to create or edit the gallery configuration in an [external editor](https://tools.duel.me/gallery-editor) with all currently supported features and paste it into the field **Advanced Gallery Settings** to apply.

To create product specific gallery settings, you need to add properties the Base Product type. These are the supported property IDs:

- duelGalleryEnabled
- duelGalleryColor
- duelGalleryBackgroundColor
- duelGalleryRows
- duelGalleryColumns
- duelGallerySortOrder
- duelGalleryLayoutStyle

## Together with the Duel Server-Side Extension

To configure which products should have a campaign and gallery, you can create a new (non-navigable) collection with all the products you wish to connect to Duel.

In Settings > Duel Settings you can then find the necessary fields for the server-side extension to retrieve the correct products and currency (Collection ID and Price Group ID). If you prefer to import the whole catalog, just leave the Collection ID field empty.

To check if everything's set up correctly, go to */ccstorex/custom/duel/products* and confirm that the JSON data's correct. You can now paste that URL in the Duel dashboard under Profile > Settings > Product catalog.